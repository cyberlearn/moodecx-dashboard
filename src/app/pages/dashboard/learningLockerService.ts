/**
 * Created by darle on 22.05.2017.
 */

import {Injectable} from "@angular/core"
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import 'rxjs/Rx'


@Injectable()
export class LearningLockerService {


  private endpoint_url = "http://10.90.129.130/data/xAPI/statements";
  private authenticationService = "MmYzZTM3YWFlMDVkNjI4YTgyNmE5NzE5MDg1MWE4MGM3NGIwYWIwMzowMTY2Njc5NDQ2ZWRiM2JkYWRlMzFmNWJkNjU4YzJhMWFmY2E4Y2Q2" ;
  // add authorization header with jwt token

  private headers = new Headers(
    { 'Authorization': 'Basic ' + this.authenticationService,
      'x-experience-api-version' : "1.0.1"
    });
  private options = new RequestOptions({ headers: this.headers });


  constructor(private http: Http){

  }

  getAllStatements (){
    return this.http.get(this.endpoint_url, this.options).map(res => res.json());
  }

  getAllRegistredAccounts (){
    return this.http.get("http://10.90.129.130/api/v1/statements/aggregate?pipeline=%5B%7B%0A%20%20%22%24match%22%3A%20%7B%20%20%22statement.verb.id%22%3A%20%22http%3A%2F%2Fadlnet.gov%2Fexpapi%2Fverbs%2Fregistered%22%20%7D%0A%7D%5D", this.options).map(res => res.json());
  }


/*
  getCourseAccess(courseid:string) {
    let parameters:string =  "?verb=http%3A%2F%2Fid.tincanapi.com%2Fverb%2Fviewed&activity=http%3A%2F%2Flocalhost%2Fmoodecx-3-1-1%2Fcourse%2Fview.php%3Fid%3D";
    //let courseid:string = "3" ;
    return this.http.get(this.endpoint_url1+parameters+courseid, this.options).map(res => res.json());
  }
  */

}
