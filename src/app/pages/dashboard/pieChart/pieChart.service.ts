import {Injectable} from '@angular/core';
import {BaThemeConfigProvider, colorHelper} from '../../../theme';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import 'rxjs/Rx'

@Injectable()
export class PieChartService {

  constructor(private _baConfig:BaThemeConfigProvider){

  }


  getData() {
    let pieColor = this._baConfig.get().colors.custom.dashboardPieChart;
    return [
      {
        color: pieColor,
        description: 'Nombre de comptes',
        stats: '',
        icon: 'face',
      }/*, {
        color: pieColor,
        description: 'Nombre de comptes',
        stats: '',
        icon: 'refresh',
      }/*, {
        color: pieColor,
        description: 'dashboard.active_users',
        stats: '178,391',
        icon: 'money',
      }, {
        color: pieColor,
        description: 'dashboard.returned',
        stats: '32,592',
        icon: 'person',
      }*/
    ];
  }

}



