import {Component} from '@angular/core';
import {PieChartService} from './pieChart.service';
import { LearningLockerService } from '../learningLockerService';

import 'easy-pie-chart/dist/jquery.easypiechart.js';

@Component({
  selector: 'pie-chart',
  templateUrl: './pieChart.html',
  styleUrls: ['./pieChart.scss'],
  providers : [LearningLockerService]
})
// TODO: move easypiechart to component
export class PieChart {

  public charts: Array<Object>;
  private _init = false;
  statements = [];
  accounts = [];
  courseAcessStatement = [] ;
  error: String = "";

  constructor(private _pieChartService: PieChartService, private LearningLockerService: LearningLockerService) {
    this.charts = this._pieChartService.getData();
    this.getAllStatements();
    this.getAllRegistredAccounts();
    this.LearningLockerService = LearningLockerService;
  }

  getAllStatements() {
    this.LearningLockerService.getAllStatements()
      .subscribe(
        data => this.statements = data.statements,
        error => this.error = "Error."
      );
  }

  getAllRegistredAccounts() {
    this.LearningLockerService.getAllRegistredAccounts()
      .subscribe(
        data => this.accounts = data.result,
        error => this.error = "Error."
      )
  }


  ngAfterViewInit() {
    if (!this._init) {
      this._loadPieCharts();
      this._updatePieCharts();
      this._init = true;
    }
  }

  private _loadPieCharts() {

    jQuery('.chart').each(function () {
      let chart = jQuery(this);
      chart.easyPieChart({
        easing: 'easeOutBounce',
        onStep: function (from, to, percent) {
          jQuery(this.el).find('.percent').text(Math.round(percent));
        },
        barColor: jQuery(this).attr('data-rel'),
        trackColor: 'rgba(0,0,0,0)',
        size: 84,
        scaleLength: 0,
        animation: 2000,
        lineWidth: 9,
        lineCap: 'round',
      });
    });
  }

  private _updatePieCharts() {
    let getRandomArbitrary = (min, max) => {
      return 5
    };

    jQuery('.pie-charts .chart').each(function (index, chart) {
      jQuery(chart).data('easyPieChart').update(getRandomArbitrary(55, 90));
    });
  }
}


