/**
 * Created by darle on 19.05.2017.
 */
// New import
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';

import { PieChart } from './pieChart';
import { PieChartService } from './pieChart/pieChart.service';

import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MoodleComponent } from './moodle.component';
import { routing } from './moodle.routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppTranslationModule,
    NgaModule,
    routing
  ],
  declarations: [
    MoodleComponent,
    PieChart
  ],
  providers:[
    PieChartService
  ]
})
export class MoodleModule {}











