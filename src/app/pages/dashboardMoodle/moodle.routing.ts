/**
 * Created by darle on 19.05.2017.
 */
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { MoodleComponent } from './moodle.component';

const routes: Routes = [
  {
    path: '',
    component: MoodleComponent,
    children: [
      //{path: 'treeview', component: TreeViewComponent }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);





